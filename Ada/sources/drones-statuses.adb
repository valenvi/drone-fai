--  drones-status.adb ---

--  Copyright 2023 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/Licenses/>.

-------------------------------------------------------------------------

with Ada.Characters.Latin_1;

package body Drones.Statuses is
    procedure Assign_Field (State : in out State_Type;
                            Name : String;
                            Value : String)
    is
        Field_Name : constant Field_Name_Type := Field_Name_Type'Value (Name);
    begin
        case Field_Name is
        when Mid =>
            State.Mid := Integer'Value (Value);
        when X =>
            State.X := Integer'Value (Value);
        when Y =>
            State.Y := Integer'Value (Value);
        when Z =>
            State.Z := Integer'Value (Value);
        when Mpry =>
            --  TODO: Implement Mpry.
            --  State.Mpry := Integer'Value (Value);
            null;
        when Pitch =>
            State.Pitch := Integer'Value (Value);
        when Roll =>
            State.Roll := Integer'Value (Value);
        when Yaw =>
            State.Yaw := Integer'Value (Value);
        when Vgx =>
            State.Vgx := Integer'Value (Value);
        when Vgy =>
            State.Vgy := Integer'Value (Value);
        when Vgz =>
            State.Vgz := Integer'Value (Value);
        when TempL =>
            State.TempL := Integer'Value (Value);
        when TempH =>
            State.TempH := Integer'Value (Value);
        when Tof =>
            State.Tof := Integer'Value (Value);
        when H =>
            State.H := Integer'Value (Value);
        when Bat =>
            State.Bat := Integer'Value (Value);
        when Baro =>
            State.Baro := Float'Value (Value);
        when Time =>
            State.Time := Integer'Value (Value);
        when Agx =>
            State.Agx := Float'Value (Value);
        when Agy =>
            State.Agy := Float'Value (Value);
        when Agz =>
            State.Agz := Float'Value (Value);
        end case;
    end Assign_Field;

    function Image (State : State_Type) return String is
        Eol : constant Character := Ada.Characters.Latin_1.LF;
    begin
        return
            "Mid: " & State.Mid'Image & Eol
            & "X: " & State.X'Image & Eol
            & "Y: " & State.Y'Image & Eol
            & "Z: " & State.Z'Image & Eol
            & "Mpry: --  TBD: Not yet implemented!" & Eol
            & "Pitch: " & State.Pitch'Image & Eol
            & "Roll: " & State.Roll'Image & Eol
            & "Yaw: " & State.Yaw'Image & Eol
            & "Vgx: " & State.Vgx'Image & Eol
            & "Vgy: " & State.Vgy'Image & Eol
            & "Vgz: " & State.Vgz'Image & Eol
            & "TempL: " & State.TempL'Image & Eol
            & "TempH: " & State.TempH'Image & Eol
            & "Tof: " & State.Tof'Image & Eol
            & "H: " & State.H'Image & Eol
            & "Bat: " & State.Bat'Image & Eol
            & "Baro: " & State.Baro'Image & Eol
            & "Time: " & State.Time'Image & Eol
            & "Agx: " & State.Agx'Image & Eol
            & "Agy: " & State.Agy'Image & Eol
            & "Agz: " & State.Agz'Image & Eol;
    end Image;

    procedure Parse_Field (State : String;
                           Start : Positive;
                           Name : out Unbounded_String;
                           Value : out Unbounded_String;
                           Next_Start : out Positive)
    is
        I : Positive := Start;
        Parsing_Name : Boolean := True;
    begin
        Name := Null_Unbounded_String;
        Value := Null_Unbounded_String;

        while I < State'Length and then State (I) /= ';' loop
            if State (I) = ':' then
                Parsing_Name := False;
            end if;

            if Parsing_Name then
                Append (Name, State (I));
            elsif not Parsing_Name and then State (I) /= ':' then
                Append (Value, State (I));
            end if;

            I := I + 1;
        end loop;

        Next_Start := I + 1;
    end Parse_Field;

    function Parse_State_String (State : String) return State_Type is
        Ret : State_Type;
        I, Next_I : Positive := 1;
        Name, Value : Unbounded_String;
        Empty : constant Unbounded_String := To_Unbounded_String ("");
    begin
        while I < State'Length loop
            Parse_Field (State, I, Name, Value, Next_I);
            if Name /= Empty and then Value /= Empty then
                Assign_Field (Ret, To_String (Name), To_String (Value));
            end if;
            I := Next_I;
        end loop;

        return Ret;
    end Parse_State_String;

end Drones.Statuses;
