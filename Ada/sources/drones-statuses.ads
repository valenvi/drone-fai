--  drones-status.ads ---

--  Copyright 2023 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------

package Drones.Statuses is

    type State_Type is tagged record
        Mid, X, Y, Z : Integer;
        --  Mpry
        Pitch, Roll, Yaw, Vgx, Vgy, Vgz,
        TempL, TempH, Tof, H, Bat : Integer;
        Baro : Float;
        Time : Integer;
        Agx, Agy, Agz : Float;
    end record;

    function Parse_State_String (State : String) return State_Type;
    --  Parse the state string provided by the Drone.
    --  It has the following syntax:
    --  "pitch:%d;roll:%d;yaw:%d;vgx:%d;vgy%d;vgz:%d;templ:%d;temph:%D;
    --  tof:%d;h:%d;bat:%d;baro:%.2f; time:%d;agx:%.2f;agy:%.2f;
    --  agz:%.2f;\r\N"

    function Parse_State_String (State : Unbounded_String)
        return State_Type
        is (Parse_State_String (To_String (State)));

    function Image (State : State_Type) return String;

private

    type Field_Name_Type is
        (Mid, X, Y, Z, Mpry,
        Pitch, Roll, Yaw, Vgx, Vgy, Vgz,
        TempL, TempH, Tof, H, Bat,
        Baro,
        Time,
        Agx, Agy, Agz);

    procedure Parse_Field (State : String;
                           Start : Positive;
                           Name : out Unbounded_String;
                           Value : out Unbounded_String;
                           Next_Start : out Positive);
    --  Parse the current field and return the name and Value.
    --
    --  Parse the string "Name:Value;" in State starting from Start.
    --  Return Name and Value as string and the end of the field at
    --  Next_Start.

    procedure Assign_Field (State : in out State_Type;
                            Name : String;
                            Value : String);
    --  Assign the field Value on State.Name.
    --
    --  Parse Value to Integer or Float depending on Name, and assign
    --  it to the corresponding field at the instance record of State.
    --  For example, Assign_Field (State, "Baro", "1.5") will assign
    --  State.Bare := Float (1.5).

end Drones.Statuses;
