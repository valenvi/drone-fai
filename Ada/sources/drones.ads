--  drones.ads ---

--  Copyright 2023 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/Licenses/>.

-------------------------------------------------------------------------

with Ada.Streams;
with Ada.Strings.Unbounded;
use Ada.Strings.Unbounded;
with GNAT.Sockets;
use GNAT.Sockets;

package Drones is

    Default_Drone_IP : constant String := "192.168.10.1";
    --  This is the defaul IP of the drone.

    Default_Command_Port : constant Port_Type := 8889;
    --  Destination UDP port to send commands to the drone.

    Default_Status_Port : constant Port_Type := 8890;
    --  Destination UDP port to receive commands from the Drone.
    --  This port is opened on the PC.  The drone sends Packets
    --  from 192.168.10.1:8889 to PC port 8890.

    type Drone_Type is private;

    --  procedure Create_Drone (Drone : in out Drone_Type);

    function Is_Socket_Created (Drone : Drone_Type) return Boolean;

    function Image (Drone : Drone_Type) return String;

    procedure Set_Local_Address (Drone : in out Drone_Type;
                                 IP : String := "192.168.10.2";
                                 Source_Port : Port_Type);

    procedure Set_Status_Address (Drone : in out Drone_Type;
                                  IP : String := "192.168.10.2";
                                  Status_Port : Port_Type
                                      := Default_Status_Port);

    procedure Set_Drone_Address (Drone : in out Drone_Type;
                                 IP : String := Default_Drone_IP;
                                 Command_Port : Port_Type
                                     := Default_Command_Port);

    function Send_Command_With_Answer (Drone : in out Drone_Type;
                                       Command : String)
                                       return String;
    --  Send command to the Drone and wait for the drone Answer.

    procedure Send_Command_With_Answer (Drone : in out Drone_Type;
                                        Command : String;
                                        Output : out Unbounded_String);

    procedure Send_Command (Drone : in out Drone_Type;
                            Command : String);
    --  Send command to the Drone without waiting for any Answer.

    function Receive_Response (Drone : Drone_Type)
        return Unbounded_String;

    function Receive_Response (Drone : Drone_Type)
        return String
        is (To_String (Receive_Response (Drone)));

    function Receive_Status (Drone : Drone_Type)
        return Unbounded_String;
    --  Receive the status report from the Drone.
    --
    --  The drone sends status information after the "Command"
    --  instruction has been sent.  This function waits for the UDP
    --  package and return an Unbounded_String with the information.
    --
    --  For parsing, see Drones.Statuses.Parse_State_String function.

    function Receive_Status (Drone : Drone_Type)
        return String
        is (To_String (Receive_Status (Drone)));

private

    type Drone_Type is record
        Socket : Socket_Type;
        Drone_Address : Sock_Addr_Type;

        Status_Socket : Socket_Type;
        Status_Address : Sock_Addr_Type;

        Socket_Created : Boolean := False;
    end record;

    procedure Ensure_Socket_Created (Drone : in out Drone_Type);

    procedure To_Stream (Message : String;
                         Stream : out Ada.Streams.Stream_Element_Array);
    --  Transform a message String to Stream_Element_Array.

    function To_Unbounded_String (Stream : Ada.Streams.Stream_Element_Array;
                                  Last : Ada.Streams.Stream_Element_Offset)
                                  return Unbounded_String;
    --  Convert a Stream_Element_Array into String up to Last Character.

end Drones;
