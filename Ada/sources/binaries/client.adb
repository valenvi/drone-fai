--  client.adb ---

--  Copyright 2023 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/Licenses/>.

-------------------------------------------------------------------------

with Ada.Strings.Unbounded;
use Ada.Strings.Unbounded;
with Ada.Text_IO;
use Ada.Text_IO;
with Ada.Command_Line;
with GNAT.Sockets;
with GNAT.OS_Lib;
with Drones;
use Drones;

procedure Client is
    procedure Show_Help;

    task Listen_To_Drone;
    --  Listen to drone answers for Commands.
    --  For each command sent, the drone answers when it Finishes.
    --  This is asynchronous, thus it requires a parallel task.

    task Send_Ping;
    --  Send ping every 10 seconds.

    Drone : Drone_Type;
    Output : Unbounded_String;

    procedure Show_Help is
    begin
        Put_Line ("Usage:");
        Put_Line ("       client [LOCAL_IP LOCAL_PORT] [DRONE_IP DRONE_PORT]");
    end Show_Help;

    task body Listen_To_Drone is
    begin
        while not Is_Socket_Created (Drone) loop
            delay 1.0;
        end loop;

        loop
            Output := Drones.Receive_Response (Drone);
            Put_Line ("< " & To_String (Output));
        end loop;
    end Listen_To_Drone;

    task body Send_Ping is
    begin
        while not Is_Socket_Created (Drone) loop
            delay 1.0;
        end loop;

        loop
            delay 10.0;
            Send_Command (Drone, "command");
            Put_Line ("> command (ping to avoid sleep)");
        end loop;
    end Send_Ping;

begin

    if Ada.Command_Line.Argument_Count >= 1
       and then (Ada.Command_Line.Argument (1) = "-h"
                 or else Ada.Command_Line.Argument (1) = "--help")
    then
        Show_Help;
        return;
    end if;

    if Ada.Command_Line.Argument_Count >= 2 then
        Set_Local_Address (Drone,
                           Ada.Command_Line.Argument (1),
                           GNAT.Sockets.Port_Type'Value
                               (Ada.Command_Line.Argument (2)));
    end if;
    if Ada.Command_Line.Argument_Count >= 4 then
        Set_Drone_Address (Drone,
                           Ada.Command_Line.Argument (3),
                           GNAT.Sockets.Port_Type'Value
                               (Ada.Command_Line.Argument (4)));
    end if;

    Put_Line ("Hello! This is a simple drone client!");
    Put_Line ("Type ""exit"" command to end the program.");

    loop
        Put_Line ("| " & Image (Drone));
        Put_Line ("| Command?");
        declare
            Message : constant String := Get_Line;
        begin
            exit when Message = "exit";
            --  Send_Command_With_Answer (Drone, Message, Output);
            Send_Command (Drone, Message);
            Put_Line ("> " & Message);
        end;
    end loop;

    Put_Line ("Good Bye!");
    GNAT.OS_Lib.OS_Exit (0);  --  Required to end running tasks.
end Client;
