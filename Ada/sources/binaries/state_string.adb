--  parse_state_string.adb ---

--  Copyright 2023 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/Licenses/>.

-------------------------------------------------------------------------

with Ada.Strings.Unbounded;
use Ada.Strings.Unbounded;
with Ada.Characters.Latin_1;
with Ada.Text_IO;
use Ada.Text_IO;
with Ada.Command_Line;
with GNAT.Sockets;
with Drones;
use Drones;
with Drones.Statuses;
use Drones.Statuses;

procedure State_String is
    procedure Show_Help;
    procedure Reset_Screen;

    procedure Reset_Screen is
        ESC : constant Character := Ada.Characters.Latin_1.ESC;
    begin
        --  A bit of magic...

        --  CSI terminal code to erase display:
        Put (ESC & "[2J");
        --  CSI terminal code to move cursor to position 0,0:
        Put (ESC & "[0;0H");

        --  I used it here:
        --  https://github.com/cnngimenez/ada-console-Utils/
        --  And more info here:
        --  http://man.he.net/man4/console_codes
    end Reset_Screen;

    procedure Show_Help is
    begin
        Put_Line ("Usage:");
        Put_Line ("       state_string [LOCAL_IP LOCAL_PORT]");
    end Show_Help;

    State : State_Type;
    State_String : Unbounded_String;
    Drone : Drone_Type;
    Not_Created_String : constant Unbounded_String :=
        To_Unbounded_String ("Socket not created");
begin
    if Ada.Command_Line.Argument_Count >= 1
       and then (Ada.Command_Line.Argument (1) = "-h"
                 or else Ada.Command_Line.Argument (1) = "--help")
    then
        Show_Help;
        return;
    end if;

    if Ada.Command_Line.Argument_Count >= 2 then
        Set_Status_Address (Drone,
                            Ada.Command_Line.Argument (1),
                            GNAT.Sockets.Port_Type'Value
                               (Ada.Command_Line.Argument (2)));
    else
        --  Use default values if no parameters are provided.
        Set_Status_Address (Drone);
    end if;

    Reset_Screen;
    loop
        Put_Line (Image (Drone));
        Put_Line ("Waiting to receive state String...");

        State_String := Receive_Status (Drone);

        if State_String = Not_Created_String then
            --  I think this is not necessary... but well...
            Put_Line ("Error creating socket.");
            return;
        end if;

        State := Drones.Statuses.Parse_State_String (State_String);

        Reset_Screen;
        Put_Line ("Results:");
        Put_Line (Image (State));
    end loop;

end State_String;
