--  client.adb ---

--  Copyright 2023 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/Licenses/>.

-------------------------------------------------------------------------

with Ada.Strings.Unbounded;
use Ada.Strings.Unbounded;
with Ada.Characters.Handling;
with Ada.Characters.Latin_1;
with Ada.Text_IO;
use Ada.Text_IO;
with Ada.Command_Line;
with GNAT.Sockets;
with GNAT.OS_Lib;
with Drones;
use Drones;
with Drones.Statuses;

procedure Control is
    procedure Show_Help;
    procedure Show_Keys;
    procedure Show_Keys_Dvorak;
    function Process_Key (C : Character) return Unbounded_String;
    function Dvorak_To_Qwerty (C : Character) return Character;

    task Listen_Status;
    --  Listen status string sent from drone to PC.

    task Listen_To_Drone;
    --  Listen to drone answers for Commands.
    --  For each command sent, the drone answers when it Finishes.
    --  This is asynchronous, thus it requires a parallel task.

    task Send_Ping;
    --  Send ping every 10 seconds.

    Use_Dvorak : Boolean := False;
    Drone : Drone_Type;
    C : Character;
    ESC : constant Character := Ada.Characters.Latin_1.ESC;
    Command : Unbounded_String;

    function Dvorak_To_Qwerty (C : Character) return Character is
        use Ada.Characters.Handling;

        Cup : Character;
    begin
        Cup := To_Upper (C);
        case Cup is
        when ',' =>
            return 'W';
        when 'O' =>
            return 'S';
        when 'A'  =>
            return 'A';
        when 'E' =>
            return 'D';
        when '-' =>
            return 'Z';
        when 'J' =>
            return 'C';
        when 'Y' =>
            return 'T';
        when 'N' =>
            return 'L';
        when 'P' =>
            return 'R';
        when 'U' =>
            return 'F';
        when 'S' =>
            return 'E';
        when others =>
            return ' ';
        end case;
    end Dvorak_To_Qwerty;

    function Process_Key (C : Character) return Unbounded_String
    is
        use Ada.Characters.Handling;

        Cup : Character;
    begin
        Cup := (if Use_Dvorak then Dvorak_To_Qwerty (C)
                    else To_Upper (C));

        case Cup is
        when 'W' =>
            return To_Unbounded_String ("forward 20");
        when 'S' =>
            return To_Unbounded_String ("back 20");
        when 'A'  =>
            return To_Unbounded_String ("left 20");
        when 'D' =>
            return To_Unbounded_String ("right 20");
        when 'Z' =>
            return To_Unbounded_String ("cw 10");
        when 'C' =>
            return To_Unbounded_String ("ccw 10");
        when 'T' =>
            return To_Unbounded_String ("takeoff");
        when 'L' =>
            return To_Unbounded_String ("land");
        when 'R' =>
            return To_Unbounded_String ("up 20");
        when 'F' =>
            return To_Unbounded_String ("down 20");
        when 'E' =>
            return To_Unbounded_String ("emergency");
        when others =>
            return Null_Unbounded_String;
        end case;
    end Process_Key;

    procedure Show_Help is
    begin
        Put_Line ("Usage:");
        Put_Line ("  control [LOCAL_IP LOCAL_PORT STATUS_PORT] "
            & "[DRONE_IP DRONE_PORT] [dvorak]");
    end Show_Help;

    procedure Show_Keys is
    begin
        Put_Line ("Keys (in QWERTY keyboards):");
        Put_Line ("w a s d: control the drone");
        Put_Line ("r f: up and down");
        Put_Line ("z c: retate clockwise and counter-clockwise");
        Put_Line ("t l: take off and land");
        Put_Line ("e: emergency stop");
        Put_Line ("q: quit the program");
    end Show_Keys;

    procedure Show_Keys_Dvorak is
    begin
        Put_Line ("Keys (in Dvorak keyboards):");
        Put_Line (", a o e: control the drone");
        Put_Line ("p u: up and down");
        Put_Line ("- j: retate clockwise and counter-clockwise");
        Put_Line ("y n: take off and land");
        Put_Line ("se emergency stop");
        Put_Line ("q: quit the program");
    end Show_Keys_Dvorak;

    task body Listen_Status is
        use Drones.Statuses;

        Output : Unbounded_String;
        Status : State_Type;
    begin
        while not Is_Socket_Created (Drone) loop
            delay 1.0;
        end loop;

        loop
            Output := Drones.Receive_Status (Drone);
            Status := Parse_State_String (Output);

            Put (ESC & "[s");
            Put (ESC & "[25;0H");
            Put (ESC & "[1J");
            Put (ESC & "[0;0H");

            Put_Line (Image (Status));
            Put (ESC & "[u");
        end loop;
    end Listen_Status;

    task body Listen_To_Drone is
        Output : Unbounded_String;
    begin
        while not Is_Socket_Created (Drone) loop
            delay 1.0;
        end loop;

        loop
            Output := Drones.Receive_Response (Drone);

            Put_Line ("< " & To_String (Output));
        end loop;
    end Listen_To_Drone;

    task body Send_Ping is
    begin
        while not Is_Socket_Created (Drone) loop
            delay 1.0;
        end loop;

        loop
            delay 10.0;
            Send_Command (Drone, "command");
            Put_Line ("> command (ping to avoid sleep)");
        end loop;
    end Send_Ping;

begin

    if Ada.Command_Line.Argument_Count >= 1
       and then (Ada.Command_Line.Argument (1) = "-h"
                 or else Ada.Command_Line.Argument (1) = "--help")
    then
        Show_Help;
        return;
    end if;

    if Ada.Command_Line.Argument_Count > 2 then
        Set_Local_Address (Drone,
                           Ada.Command_Line.Argument (1),
                           GNAT.Sockets.Port_Type'Value
                               (Ada.Command_Line.Argument (2)));
        Set_Status_Address (Drone,
                            Ada.Command_Line.Argument (1),
                            GNAT.Sockets.Port_Type'Value
                               (Ada.Command_Line.Argument (3)));
    end if;
    if Ada.Command_Line.Argument_Count >= 4 then
        Set_Drone_Address (Drone,
                           Ada.Command_Line.Argument (4),
                           GNAT.Sockets.Port_Type'Value
                               (Ada.Command_Line.Argument (5)));
    end if;

    if Ada.Command_Line.Argument_Count >= 6 then
        Use_Dvorak := True;
    end if;

    Put_Line ("Hello! This is a simple drone remote control!");
    if Use_Dvorak then
        Show_Keys_Dvorak;
    else
        Show_Keys;
    end if;

    Put_Line ("Press key to start.");
    Get_Immediate (C);
    Put (ESC & "[2J");
    Put (ESC & "[25;0H");

    Send_Command (Drone, "command");
    Put_Line ("> command");

    loop
        Get_Immediate (C);

        Command := Process_Key (C);

        if Command /= Null_Unbounded_String then
            declare
                Command_String : constant String := To_String (Command);
            begin
                Send_Command (Drone, Command_String);
                Put_Line ("> " & Command_String);
            end;
        end if;

        exit when C = 'Q' or else C = 'q';
    end loop;

    Send_Command (Drone, "land");

    Put_Line ("Good Bye!");
    GNAT.OS_Lib.OS_Exit (0);  --  Required to end running tasks.
end Control;
