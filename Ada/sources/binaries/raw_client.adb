--  client.adb ---

--  Copyright 2023 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/Licenses/>.

-------------------------------------------------------------------------

with Ada.Streams;
with Ada.Text_IO;
with Ada.Strings.Unbounded;
with Ada.Command_Line;
use Ada.Command_Line;
with GNAT.Sockets;
use GNAT.Sockets;

procedure Client is
    procedure To_Stream (Message : String;
                         Stream : out Ada.Streams.Stream_Element_Array);
    function To_String (Stream : Ada.Streams.Stream_Element_Array;
                        Last : Ada.Streams.Stream_Element_Offset)
                        return String;

    procedure To_Stream (Message : String;
                         Stream : out Ada.Streams.Stream_Element_Array)
    is
        use Ada.Streams;
        I_Stream : Stream_Element_Offset := 0;
    begin
        for I in 1 .. Message'Length loop
            I_Stream := I_Stream + 1;
            Stream (I_Stream) := Character'Pos (Message (I));
        end loop;
    end To_Stream;

    function To_String (Stream : Ada.Streams.Stream_Element_Array;
                        Last : Ada.Streams.Stream_Element_Offset)
                        return String
    is
        use Ada.Strings.Unbounded;

        Temp : Unbounded_String;
    begin
        for I in 1 .. Last loop
            Append (Temp, Character'Val (Stream (I)));
        end loop;

        return To_String (Temp);
    end To_String;

    Drone_IP : constant String := "192.168.10.1";
    --  Drone_IP : constant String := "127.0.0.1";
    Drone_Port : constant Port_Type := 8889;

    Address : Sock_Addr_Type;
    Socket : Socket_Type;

    Data : Ada.Streams.Stream_Element_Array (1 .. 512);
    Last : Ada.Streams.Stream_Element_Offset;

begin
    if Argument_Count < 1 then
        return;
    end if;

    Address.Addr := Inet_Addr (Drone_IP);
    Address.Port := Drone_Port;

    Create_Socket (Socket, Family_Inet, Socket_Datagram);

    Ada.Text_IO.Put_Line ("| Sending to (UDP):" & Image (Address));
    Ada.Text_IO.Put_Line ("> " & Argument (1));
    declare
        Data_Send : Ada.Streams.Stream_Element_Array
            (1 .. Argument (1)'Length);
    begin
        To_Stream (Argument (1), Data_Send);
        Send_Socket (Socket, Data_Send, Last, Address);
    end;
    Ada.Text_IO.Put_Line ("| Sent " & Last'Image & " bytes.");

    Receive_Socket (Socket, Data, Last, Address);
    Ada.Text_IO.Put_Line ("< "
        & To_String (Data, Last));
end Client;
