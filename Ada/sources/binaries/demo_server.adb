with Ada.Text_IO;
with Ada.Characters.Latin_1;
with GNAT.Sockets;
use GNAT.Sockets;

procedure Demo_Server is
    Server_Port : constant Port_Type := 8890;
    --  Puerto del drone a simular.

    Address : Sock_Addr_Type;
    Server : Socket_Type;
    Channel : Stream_Access;

    EOL : constant Character := Ada.Characters.Latin_1.LF;

    Process_Messages : Boolean := True;
begin
    Ada.Text_IO.Put_Line ("Welcome to Drone Demo Server!");
    Ada.Text_IO.Put_Line ("| Starting Server...");

    --  Address.Addr := Addresses (Get_Host_By_Name (Host_Name), 1);
    Address.Addr := Any_Inet_Addr;
    Address.Port := Server_Port;
    Create_Socket (Server, Family_Inet, Socket_Datagram);

    Set_Socket_Option (Server, Socket_Level, (Reuse_Address, True));
    Set_Socket_Option (Server, Socket_Level, (Receive_Timeout, Timeout => 1.0));

    Bind_Socket (Server, Address);

    Ada.Text_IO.Put_Line ("| Server socket opened.");
    Ada.Text_IO.Put_Line ("| " & Image (Address));
    Ada.Text_IO.Put_Line ("| Waiting for Clients.");

    Ada.Text_IO.Put_Line ("|-Accepted Client.");

    delay 0.2;

    while Process_Messages loop
        declare
            Message : String := String'Input (Channel);
        begin
            Ada.Text_IO.Put_Line ("< " & Message);
            Ada.Text_IO.Put_Line ("> ok");
            String'Output (Channel, "ok" & EOL);
        end;
    end loop;

    Ada.Text_IO.Put_Line ("| Closing server.");
    Close_Socket (Server);

    Ada.Text_IO.Put_Line ("Good bye!");
end Demo_Server;
