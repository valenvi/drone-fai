--  drones.adb ---

--  Copyright 2023 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/Licenses/>.

-------------------------------------------------------------------------

package body Drones is

    procedure Ensure_Socket_Created (Drone : in out Drone_Type) is
    begin
        if Drone.Socket_Created then
            return;
        end if;

        Drone.Drone_Address.Addr := Inet_Addr (Default_Drone_IP);
        Drone.Drone_Address.Port := Default_Command_Port;

        Create_Socket (Drone.Socket, Family_Inet, Socket_Datagram);
        Create_Socket (Drone.Status_Socket, Family_Inet, Socket_Datagram);

        Drone.Socket_Created := True;
    end Ensure_Socket_Created;

    function Image (Drone : Drone_Type) return String is
    begin
        if not Drone.Socket_Created then
            return "Drone: Socket not created.";
        end if;

        return "Local IP/Port: "
            & Image (Get_Socket_Name (Drone.Socket))
            & " | Drone IP/Port: "
            & Image (Drone.Drone_Address)
            & " | Status IP/Port: "
            & Image (Get_Socket_Name (Drone.Status_Socket));
    end Image;

    function Is_Socket_Created (Drone : Drone_Type) return Boolean
    is
    begin
        return Drone.Socket_Created;
    end Is_Socket_Created;

    function Receive_Response (Drone : Drone_Type)
        return Unbounded_String
    is
        Address : Sock_Addr_Type;
        Data : Ada.Streams.Stream_Element_Array (1 .. 512);
        Last : Ada.Streams.Stream_Element_Offset;
    begin
        if not Drone.Socket_Created then
            return To_Unbounded_String ("Socket not created");
        end if;

        Receive_Socket (Drone.Socket, Data, Last, Address);

        return To_Unbounded_String (Data, Last);
    end Receive_Response;

    function Receive_Status (Drone : Drone_Type)
        return Unbounded_String
    is
        Address : Sock_Addr_Type;
        Data : Ada.Streams.Stream_Element_Array (1 .. 512);
        Last : Ada.Streams.Stream_Element_Offset;
    begin
        if not Drone.Socket_Created then
            return To_Unbounded_String ("Socket not created");
        end if;

        --  We need to bind the port to the Status_Socket.  It Will
        --  never send anything to the server, thus it will not Know
        --  which port to Listen.
        --  Bind_Socket (Drone.Status_Socket, Drone.Status_Address);
        Receive_Socket (Drone.Status_Socket, Data, Last, Address);

        return To_Unbounded_String (Data, Last);
    end Receive_Status;

    procedure Send_Command (Drone : in out Drone_Type;
                            Command : String)
    is
        Last : Ada.Streams.Stream_Element_Offset;
    begin
        Ensure_Socket_Created (Drone);

        declare
            Data_Send : Ada.Streams.Stream_Element_Array
                (1 .. Command'Length);
        begin
            To_Stream (Command, Data_Send);
            Send_Socket (Drone.Socket, Data_Send, Last,
                         Drone.Drone_Address);
        end;
    end Send_Command;

    procedure Send_Command_With_Answer (Drone : in out Drone_Type;
                                        Command : String;
                                        Output : out Unbounded_String)
    is
        Address : Sock_Addr_Type;

        Data : Ada.Streams.Stream_Element_Array (1 .. 512);
        Last : Ada.Streams.Stream_Element_Offset;
    begin
        Ensure_Socket_Created (Drone);

        declare
            Data_Send : Ada.Streams.Stream_Element_Array
                (1 .. Command'Length);
        begin
            To_Stream (Command, Data_Send);
            Send_Socket (Drone.Socket, Data_Send, Last,
                         Drone.Drone_Address);
        end;

        Receive_Socket (Drone.Socket, Data, Last, Address);

        Output := To_Unbounded_String (Data, Last);
    end Send_Command_With_Answer;

    function Send_Command_With_Answer (Drone : in out Drone_Type;
                                       Command : String)
                                       return String
    is
        Output : Unbounded_String;
    begin
        Send_Command_With_Answer (Drone, Command, Output);
        return To_String (Output);
    end Send_Command_With_Answer;

    procedure Set_Drone_Address (Drone : in out Drone_Type;
                                 IP : String := Default_Drone_IP;
                                 Command_Port : Port_Type
                                     := Default_Command_Port)
    is
    begin
        Drone.Drone_Address.Addr := Inet_Addr (IP);
        Drone.Drone_Address.Port := Command_Port;
    end Set_Drone_Address;

    procedure Set_Local_Address (Drone : in out Drone_Type;
                                 IP : String := "192.168.10.2";
                                 Source_Port : Port_Type)
    is
        Address : Sock_Addr_Type;
    begin
        Ensure_Socket_Created (Drone);

        Address.Addr := Inet_Addr (IP);
        Address.Port := Source_Port;

        Bind_Socket (Drone.Socket, Address);
    end Set_Local_Address;

    procedure Set_Status_Address (Drone : in out Drone_Type;
                                  IP : String := "192.168.10.2";
                                  Status_Port : Port_Type
                                      := Default_Status_Port)
    is
    begin
        Ensure_Socket_Created (Drone);

        Drone.Status_Address.Addr := Inet_Addr (IP);
        Drone.Status_Address.Port := Status_Port;

        Bind_Socket (Drone.Status_Socket, Drone.Status_Address);
    end Set_Status_Address;

    procedure To_Stream (Message : String;
                         Stream : out Ada.Streams.Stream_Element_Array)
    is
        use Ada.Streams;
        I_Stream : Stream_Element_Offset := 0;
    begin
        for I in 1 .. Message'Length loop
            I_Stream := I_Stream + 1;
            Stream (I_Stream) := Character'Pos (Message (I));
        end loop;
    end To_Stream;

    function To_Unbounded_String (Stream : Ada.Streams.Stream_Element_Array;
                                  Last : Ada.Streams.Stream_Element_Offset)
                                  return Unbounded_String
    is
        Temp : Unbounded_String := To_Unbounded_String ("");
    begin
        for I in 1 .. Last loop
            Append (Temp, Character'Val (Stream (I)));
        end loop;

        return Temp;
    end To_Unbounded_String;

end Drones;
