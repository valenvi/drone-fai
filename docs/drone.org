
* Conexión

** Wi-Fi
El dron Tello posee su propio Access Point, por lo que puede generar una red Wi-Fi de corta distancia con el cual conectarse. 

** IP y UDP
En la red Wi-Fi, La IP del dron es fija en 192.168.10.1. Responde a cualquier ping que se le realice a esta dirección. La PC u otro dispositivo, al conectarse, adquiere una IP automáticamente (posiblemente por medio de un servidor DHCP) a partir del 192.168.10.2 en adelante.

En la capa de transporte utiliza dos puertos UDP: 8889 y 8890. El primer puerto se abre en el dron para recibir comandos en texto plano. El segundo, debe utilizarlo la PC para recibir el estado del

Recordar que: al enviar la intrucción "command" por UDP, *el drone utilizará el mismo puerto fuente de la PC para responder esa y cualquier sucesiva instrucción*.

*** Captura del estado del dron
El dron envía a la PC un string con el estado actual luego de recibir la instrucción "command". Este string es enviado sucesivamente una vez cada 0.1 segundos aproximadamente.
En el Listado [[src-status-udp][src-status-udp]] se muestra la salida de una captura realizada con Wireshark (véase la Figura [[fig:status-wireshark][fig:status-wireshark]]) y volcada a la terminal con el comando =tcpdump -r captura-dron.pcapng -n -v -X=. 

#+name: fig:status-wireshark
#+caption: Wireshark detenido luego de una captura de paquetes UDP desde el dron.
[[file:imgs/Screenshot-Wireshark.png]]

El programa tcpdump también puede ser utilizado para capturar paquetes. Suponiendo que "wlo1" es el nombre de la interfaz Wi-Fi de la computadora, el siguiente comando inicia la captura de paquetes. Para terminar su ejecución y la captura se debe presionar Control C.

: tcpdump -w captura-dron.pcapng -i wlo1 udp port 8890

#+name: src-status-udp
#+caption: Captura con Wireshark y tcpdump de paquetes UDP desde el dron.
#+BEGIN_SRC pcap
17:40:33.387312 IP (tos 0x0, ttl 255, id 156, offset 0, flags [none], proto UDP (17), length 187)
    192.168.10.1.8889 > 192.168.10.2.8890: UDP, length 159
	0x0000:  4500 00bb 009c 0000 ff11 2542 c0a8 0a01  E.........%B....
	0x0010:  c0a8 0a02 22b9 22ba 00a7 9ff7 6d69 643a  ....".".....mid:
	0x0020:  3235 373b 783a 303b 793a 303b 7a3a 303b  257;x:0;y:0;z:0;
	0x0030:  6d70 7279 3a30 2c30 2c30 3b70 6974 6368  mpry:0,0,0;pitch
	0x0040:  3a30 3b72 6f6c 6c3a 303b 7961 773a 303b  :0;roll:0;yaw:0;
	0x0050:  7667 783a 303b 7667 793a 303b 7667 7a3a  vgx:0;vgy:0;vgz:
	0x0060:  303b 7465 6d70 6c3a 3932 3b74 656d 7068  0;templ:92;temph
	0x0070:  3a39 343b 746f 663a 3130 3b68 3a30 3b62  :94;tof:10;h:0;b
	0x0080:  6174 3a37 383b 6261 726f 3a34 3331 2e32  at:78;baro:431.2
	0x0090:  383b 7469 6d65 3a30 3b61 6778 3a2d 3132  8;time:0;agx:-12
	0x00a0:  2e30 303b 6167 793a 382e 3030 3b61 677a  .00;agy:8.00;agz
	0x00b0:  3a2d 3939 382e 3030 3b0d 0a              :-998.00;..
17:40:33.489398 IP (tos 0x0, ttl 255, id 157, offset 0, flags [none], proto UDP (17), length 187)
    192.168.10.1.8889 > 192.168.10.2.8890: UDP, length 159
	0x0000:  4500 00bb 009d 0000 ff11 2541 c0a8 0a01  E.........%A....
	0x0010:  c0a8 0a02 22b9 22ba 00a7 a1f8 6d69 643a  ....".".....mid:
	0x0020:  3235 373b 783a 303b 793a 303b 7a3a 303b  257;x:0;y:0;z:0;
	0x0030:  6d70 7279 3a30 2c30 2c30 3b70 6974 6368  mpry:0,0,0;pitch
	0x0040:  3a30 3b72 6f6c 6c3a 303b 7961 773a 303b  :0;roll:0;yaw:0;
	0x0050:  7667 783a 303b 7667 793a 303b 7667 7a3a  vgx:0;vgy:0;vgz:
	0x0060:  303b 7465 6d70 6c3a 3932 3b74 656d 7068  0;templ:92;temph
	0x0070:  3a39 343b 746f 663a 3130 3b68 3a30 3b62  :94;tof:10;h:0;b
	0x0080:  6174 3a37 383b 6261 726f 3a34 3331 2e32  at:78;baro:431.2
	0x0090:  373b 7469 6d65 3a30 3b61 6778 3a2d 3131  7;time:0;agx:-11
	0x00a0:  2e30 303b 6167 793a 372e 3030 3b61 677a  .00;agy:7.00;agz
	0x00b0:  3a2d 3939 382e 3030 3b0d 0a              :-998.00;..
#+END_SRC

De la misma manera, el programa netcat puede ser utilizado para mostrar en terminal el string recibido:

: netcat -l -p 8890

*** Envío de comandos al dron
Una forma sencilla de enviar comandos al dron es por medio de la terminal. Para ello, es necesario el programa netcat, que permite enviar texto o datos desde la entrada estándar a un puerto TCP o UDP. 

La siguiente línea envía la instrucción "command" al dron. Observar que "echo" recibe el parámetro "-n" para no agregar un fin de línea al final ("\n"). En cuanto al parámetro "-u" de netcat, le indica que utilice el protocolo UDP; y "-p 12345" le especifica que utilice el puerto fuente de la PC 12345. 

: echo -n "command" | netcat -p 12345 -u 192.168.10.1 8889

Al ejecutarse, netcat envía el comando y luego queda en espera, escuchando en el mismo puerto cualquier respuesta. Como el dron solo responde "ok" o "error" y UDP no utiliza conexiones (no se puede detectar cuándo el dron cierra la conexión por su cuenta), es preciso terminar el programa con Control C.

Es necesario especificar un puerto fuente y no dejar que el sistema operativo de la PC seleccione uno aleatorio. Esto es porque el dron utilizará ese puerto para enviar cualquier respuesta, sea del comando actual como de cualquier otro comando posterior. Es por ello que debe asignarse un puerto con "-p PUERTO" a netcat.

También, se sugiere enviar una instrucción de consulta, o el mismo "command", una vez cada 10 o 15 segundos. En caso de no hacerlo, el dron aterrizará y puede comenzar el modo suspensión, desconectando el Wi-Fi y apagándose para ahorrar batería. 

* Implementación en Ada
En el lenguaje de programación Ada, se implementaron programas y bibliotecas para enviar comandos, recibir sus respuestas y recibir la información de estado. Ambos elementos pueden compilarse en modo dinámico (o "relocatable") o estático. En el primer caso, los programas dinámicos requieren de bibliotecas instaladas del sistema; archivos de extensión =.so= usualmente bajo el directorio =/lib/= o =/usr/lib=. En el segundo caso, los programas incluyen todas las bibliotecas necesarias dentro del archivo generado.

** Compilación
Para compilar se requiere de GNAT 2012 ó 202x (usualmente incluído en GCC) y GPRBuild instalado. Además, la herramienta =make= debe estar disponible en la terminal.

Para iniciar la compilación, se debe tipear =make= en el directorio =./Ada=.

** Bibliotecas
Existen dos bibliotecas implementadas:

- Drones :: Contiene estructuras, procedimientos y funciones para conectarse con el dron para enviar y recibir datos.
- Drones.Statuses :: Permite interpretar el string de estado enviado por el dron, y generar una estructura utilizable por el lenguaje Ada.

*** Drones
En el archivo =drones.ads= puede leerse la interfaz disponible para utilizarse. Aquí se define una estructura =Drone_Type= que encapsula la conexión UDP, tanto para enviar comandos como para recibir el estado. De esta manera, el usuario de esta interfaz no requiere inicializar los sockets UDP, puesto que los realiza la biblioteca.

*** Drones.Statuses
Aquí se define =State_Type=, una estructura cuyos campos pueden llenarse con datos utilizando la función =Parse_State_String=. La estructura no es privada; se encuentra abierta para poder acceder a cada campo independientemente, y realizar caculos con ellos.

El método habitual para obtener los datos es el siguiente:

#+BEGIN_SRC alight
  procedure Status_Parsing_Example (Drone : in out Drones.Drone_Type) is    
      Status_String : Ada.Strings.Unbounded.Unbounded_String;
      State : Drones.Statuses.State_Type;
  begin  
      Status_String := Drones.Receive_Status (Drone);
      State := Drones.Statuses.Parse_State_string (Status_String);
      --  ...
  end Status_Parsing_Example;
#+END_SRC


* Información y documentación
- Recursos y manuales del Software Development Kit (SDK) pueden encontrarse en: https://www.ryzerobotics.com/tello-edu/downloads

- Códigos de ejemplo en: https://github.com/dji-sdk/Tello-Python/blob/master/tello_state.py

- Manpages de tcpdump(1) y pcap-filter(7)

- Manpage de netcat(1)

* Meta     :noexport:

# ----------------------------------------------------------------------
#+TITLE:  Drone Tello
#+SUBTITLE:
#+AUTHOR: Christian Gimenez
#+DATE:   11 nov 2023
#+EMAIL:
#+DESCRIPTION: 
#+KEYWORDS: 
#+COLUMNS: %40ITEM(Task) %17Effort(Estimated Effort){:} %CLOCKSUM

#+STARTUP: inlineimages hidestars content hideblocks entitiespretty
#+STARTUP: indent fninline latexpreview

#+OPTIONS: H:3 num:t toc:t \n:nil @:t ::t |:t ^:{} -:t f:t *:t <:t
#+OPTIONS: TeX:t LaTeX:t skip:nil d:nil todo:t pri:nil tags:not-in-toc
#+OPTIONS: tex:imagemagick

#+TODO: TODO(t!) CURRENT(c!) PAUSED(p!) | DONE(d!) CANCELED(C!@)

# -- Export
#+LANGUAGE: en
#+LINK_UP:   
#+LINK_HOME: 
#+EXPORT_SELECT_TAGS: export
#+EXPORT_EXCLUDE_TAGS: noexport
# #+export_file_name: index

# -- HTML Export
#+INFOJS_OPT: view:info toc:t ftoc:t ltoc:t mouse:underline buttons:t path:libs/org-info.js
#+HTML_LINK_UP: index.html
#+HTML_LINK_HOME: index.html
#+XSLT:

# -- For ox-twbs or HTML Export
# #+HTML_HEAD: <link href="libs/bootstrap.min.css" rel="stylesheet">
# -- -- LaTeX-CSS
# #+HTML_HEAD: <link href="css/style-org.css" rel="stylesheet">

# #+HTML_HEAD: <script src="libs/jquery.min.js"></script> 
# #+HTML_HEAD: <script src="libs/bootstrap.min.js"></script>


# -- LaTeX Export
# #+LATEX_CLASS: article
#+latex_compiler: xelatex
# #+latex_class_options: [12pt, twoside]

#+latex_header: \usepackage{csquotes}
# #+latex_header: \usepackage[spanish]{babel}
# #+latex_header: \usepackage[margin=2cm]{geometry}
# #+latex_header: \usepackage{fontspec}
# -- biblatex
#+latex_header: \usepackage[backend=biber, style=alphabetic, backref=true]{biblatex}
#+latex_header: \addbibresource{tangled/biblio.bib}
# -- -- Tikz
# #+LATEX_HEADER: \usepackage{tikz}
# #+LATEX_HEADER: \usetikzlibrary{arrows.meta}
# #+LATEX_HEADER: \usetikzlibrary{decorations}
# #+LATEX_HEADER: \usetikzlibrary{decorations.pathmorphing}
# #+LATEX_HEADER: \usetikzlibrary{shapes.geometric}
# #+LATEX_HEADER: \usetikzlibrary{shapes.symbols}
# #+LATEX_HEADER: \usetikzlibrary{positioning}
# #+LATEX_HEADER: \usetikzlibrary{trees}

# #+LATEX_HEADER_EXTRA:

# --  Info Export
#+TEXINFO_DIR_CATEGORY: A category
#+TEXINFO_DIR_TITLE: Drone Tello: (drone)
#+TEXINFO_DIR_DESC: One line description.
#+TEXINFO_PRINTED_TITLE: Drone Tello
#+TEXINFO_FILENAME: drone.info


# Local Variables:
# org-hide-emphasis-markers: t
# org-use-sub-superscripts: "{}"
# fill-column: 80
# visual-line-fringe-indicators: t
# ispell-local-dictionary: "british"
# org-latex-default-figure-position: "tbp"
# End:
